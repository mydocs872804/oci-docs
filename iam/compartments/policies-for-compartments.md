# policies for compartments

**OCI had built-in policy for admins**\
![](<../../.gitbook/assets/image (4).png>)

### policy inheritance

![policy inheritance](<../../.gitbook/assets/image (6).png>)

![](<../../.gitbook/assets/image (3).png>)

```
// allow group admins to manage <> in compartment A:B:C

we need to write the whole path (A:B:C) to manage resources in compartment c
```

