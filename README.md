---
description: Identity and access management
---

# 🔐 IAM

### Basics

role based access controls

<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td>authN</td><td>who are you</td><td></td></tr><tr><td>authZ</td><td>what permission do u have</td><td></td></tr></tbody></table>

policies are attached to groups --> users

OCID --> orcle cloud id&#x20;

ocid1.\<resourcetyps.\<realm>.\[region].\<uniqid>

<img src=".gitbook/assets/file.drawing.svg" alt="" class="gitbook-drawing">

ocid1.tenacy.oci..\<uniqid>

ocid1.volume1.oc1.eu-frank.\<id>

### Identity Domains

default domain - free domain

|                     |                                                                |
| ------------------- | -------------------------------------------------------------- |
| free                | limit of 2000 users                                            |
| oracle apps premium | <p>authN to oracle apps<br>manage oracle apps on the cloud</p> |
| premium             |                                                                |
| external user       |                                                                |

policies sit outside of domains

policy  ------> refrence identity domains

Group --> collection of users

#### AuthN

1. Username and pwd
2. API signing key

Authtokens -- oracle generated token strings  ---> third party apps

#### AuthZ

role based access control

> by default everything is denied ( least privilege )
