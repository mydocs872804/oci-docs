# 🚆 compartments

<mark style="color:red;">Tenancy (ROOT Compartment)</mark>\
![oci compartment](<../../.gitbook/assets/image (5).png>)\
\
compartment --> logical constraint

1. Each resource **belongs to a **<mark style="color:red;">**single**</mark>** compartment**
2. resources from multiple regions can be in the same compartment \
   <mark style="color:red;">compartment is global resource.</mark>\
   \
   ![region-compartments](<../../.gitbook/assets/image (9).png>)
3. nested compartments - **six levels** of nested compartments.
4. set quotas and budgets on compartments

* when account is opened we have a root compartment (Default) created&#x20;

![root compartment](../../.gitbook/assets/image.png)

> Governance & administration --> Tenancy Explorer -- all resources in the compartments

### Compartment quotas service limits

Governance & administration --> Tenancy Explorer --> limits quotas and usage\
\
[https://docs.oracle.com/en-us/iaas/Content/Quotas/Concepts/resourcequotas.htm](https://docs.oracle.com/en-us/iaas/Content/Quotas/Concepts/resourcequotas.htm)

> create quota policies to restrict creation of resources in compartment scope\
>
>
> ```
> set compute-core quota standard2-core-count to 10 in tenancy
> unset compute-core quota standard2-core-count in compartment productionApp
> ```

### Budgets for compartments

Billing& cost management --> Budgets

{% embed url="https://blogs.oracle.com/cloud-infrastructure/post/best-practices-for-compartments" %}
best practices for compartments
{% endembed %}

