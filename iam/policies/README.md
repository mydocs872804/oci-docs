# 📖 policies

> policies you write for groups

{% hint style="info" %}
dynamic-group ( service )
{% endhint %}

### subject clause

write identity domains in the subject, if you are not mentioning it takes default domain

### Action clause

* inspect - details of object
* read
* use (write)
* manage ( delete )

Individual resource type\
aggregate resource type
