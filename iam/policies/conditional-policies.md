# conditional policies

![](<../../.gitbook/assets/image (8).png>)

```
Allow group GroupAdmins to use users in tenancy 
 where target.group.name != 'Administrators'

```

{% embed url="https://docs.oracle.com/en-us/iaas/Content/Identity/Concepts/policyadvancedfeatures.htm" %}
advanced policy features
{% endembed %}

### network resources

<mark style="color:red;">where request.networkSource.name = 'corpnet'</mark>

> we can control access on originating ip address&#x20;

identity --> Network resources \
\
\>> it sits outside the Domains
