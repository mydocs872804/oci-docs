# dynamic groups

<table><thead><tr><th width="280">Term</th><th width="454.3333333333333">Def</th></tr></thead><tbody><tr><td>Principal</td><td>Identity of the caller trying to access on a resource</td></tr><tr><td>User</td><td>Represent a human in the org</td></tr><tr><td>Instance</td><td>compute vm host in a tenancy</td></tr><tr><td>Service</td><td>app developed by oci , that an end user can use</td></tr><tr><td>Resource</td><td>similar to instance ( e.g., load balancer)</td></tr></tbody></table>

### Resource Principals Patters :-

| principal                 | desc                                                                            | example                  |
| ------------------------- | ------------------------------------------------------------------------------- | ------------------------ |
| Infrastructure Principals | that enables instances to be authZ to perform some actions                      | e.g., Instance principal |
| stacked principal         | <p>service controlling resource<br>specifies the intention of the  resource</p> | oracle db                |
| ephemeral principal       | temporary cred's for a short period                                             | oracle function          |

**Dynamic groups** allow you to group Oracle Cloud Infrastructure compute instances as "principal" actors (similar to user groups)\
\
When you create a dynamic group, rather than adding members explicitly to the group, you instead define a set of _**matching rules**_ to define the group members. For example, a rule could specify that all instances in a particular compartment are members of the dynamic group. The members can change **dynamically as instances are launched and terminated in that compartment**

### work with Dynamic groups -->

#### AuthN

Create dynamic group&#x20;

{% embed url="https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/managingdynamicgroups.htm" %}
creation of dynamic groups
{% endembed %}

#### AuthZ

adding policies&#x20;

{% embed url="https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/callingservicesfrominstances.htm#Writing" %}
policies for dynamic groups
{% endembed %}

### Limits on Dynamic Groups <a href="#ariaid-title7" id="ariaid-title7"></a>

A single compute instance can belong to a <mark style="color:red;">maximum of 5 dynamic groups</mark>.

You can have a <mark style="color:red;">maximum of 50 dynamic groups</mark> in your tenancy.
